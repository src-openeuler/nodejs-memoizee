%{?nodejs_find_provides_and_requires}
%global packagename memoizee
%global enable_tests 0
Name:		nodejs-memoizee
Version:	0.3.9
Release:	2
Summary:	Memoize/cache function results
License:	MIT
URL:		https://github.com/medikoo/memoizee
Source0:	https://registry.npmjs.org/%{packagename}/-/%{packagename}-%{version}.tgz
BuildArch:	noarch
ExclusiveArch:       %{nodejs_arches} noarch
BuildRequires:       	nodejs-packaging
%if 0%{?enable_tests}
BuildRequires:       	npm(tad)
%endif
%description
Memoize/cache function results

%prep
%setup -q -n package
%nodejs_fixdep es6-weak-map ~2.0.1
%nodejs_fixdep d "<=2.0.0"
%nodejs_fixdep next-tick ^1.1.0

%build

%install
mkdir -p %{buildroot}%{nodejs_sitelib}/%{packagename}
cp -pr package.json *.js lib/ ext/ normalizers/ \
	%{buildroot}%{nodejs_sitelib}/%{packagename}
%nodejs_symlink_deps
%if 0%{?enable_tests}

%check
%nodejs_symlink_deps --check
%{__nodejs} -e 'require("./")'
%{__nodejs} %{nodejs_sitelib}/tad/bin/tad
%endif

%files
%{!?_licensedir:%global license %doc}
%doc *.md
%license LICENSE
%{nodejs_sitelib}/%{packagename}

%changelog
* Sat Jul 02 2022 wangkai <wangkai385@h-partners.com> - 0.3.9-2
- Modify require version to fix unresolvable

* Mon Aug 17 2020 wutao <wutao61@huawei.com> - 0.3.9-1
- Package init
